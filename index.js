function countLetter(letter, sentence) {
    let result = 0;

    const lowercaseLetter = letter.toLowerCase();
    const lowercaseSentence = sentence.toLowerCase();

    if (typeof letter !== 'string' || letter.length !== 1) {
        return undefined;
    } else {
        for (let i = 0; i < lowercaseSentence.length; i++) {
            if (lowercaseSentence[i] === lowercaseLetter) {
                result++;
            }
        }
    } 

    return result;
}


function isIsogram(text) {
    
    const lowercaseText = text.toLowerCase();
    const letterSet = new Set();

    for (let i = 0; i < lowercaseText.length; i++) {
        const letter = lowercaseText[i];

        if (letterSet.has(letter)) {
            return false;
        }
        letterSet.add(letter);
    }
    return true;
}


function purchase(age, price) {
    
    const discountedPrice = price * 0.8;
    const roundedDiscountedPrice = discountedPrice.toFixed(2);

    if(age < 13) {
        return undefined
    } else if(age >= 13 && age <= 21) {
        const discountedPrice = price * 0.8;
        const roundedDiscountedPrice = discountedPrice.toFixed(2);
        return roundedDiscountedPrice;
    } else if(age >= 22 && age <= 64) {
        const roundedPrice = price.toFixed(2);
        return roundedPrice;
    }
    return roundedDiscountedPrice; 
}


function findHotCategories(items) {

    const hotCategories = items.filter(item => item.stocks === 0);
    const uniqueHotCategories = [...new Set(hotCategories.map(item => item.category))];

    return uniqueHotCategories;
}
const items = [
    { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
    { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
    { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
    { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
    { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
];


function findFlyingVoters(candidateA, candidateB) {
    
    const flyingVoters = candidateA.filter(voter => candidateB.includes(voter));
    return flyingVoters;
}

const candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'];
const candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l'];



module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};